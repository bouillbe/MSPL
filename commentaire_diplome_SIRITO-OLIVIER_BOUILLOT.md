Notre jeu de données donnent le nombre de personnes par regions/departement/villes en 2016, 
qui ont un diplome, de plus chaque données est partagé selon le sexe de la personne, et 
son niveau d'etudes.

De ce fait nous avons définis 1 question principale : 
1) Est-ce que les personnes venant de la region parisienne sont plus diplomés que les provinciaux ?

Ainsi, 2 autres questions complementaires peuvent devenir interessantes : 
2) Est-ce que les femmes sont plus diplomées que les hommes ? 
3) Quelle est la region ou il y a le plus fort taux de diplomés d'etudes superieurs ? Est-ce la region parisienne ?

Ces données viennent de l'insee (https://insee.fr/fr/statistiques/1893149), 
elles ont été recolté durant le recensement de l'année 2016.

Pour repondre a la premiere questions on peut imaginer, 
un histogramme avec un baton par regions representant le nombre de diplomés d'etudes superieurs.

Pour repondre a la deuxieme question, nous pouvons faire un histogramme, 
avec le nombre de femmes diplomés d'etude superieur, et le nombre d'homme diplomés d'etudes superieur, 
ainsi nous pourront comparés les 2.

Mais ceci ne nous aide en rien, de ce fait, nous devons calculés le taux de pourcentage de diplomés par region, 
et comparés avec les autres regions. 

De ce fait, nous pourrions repondre a une question proche de la question 1, "est-ce que la region parisienne a le plus diplomés ?"


Pour informartion, la region parisienne represente les departements : 
75, 77, 78, 91, 92, 93, 94, 95, et est representée par le code INSEE 11.